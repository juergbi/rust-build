use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    tonic_build::configure()
        .type_attribute(
            "build.bazel.remote.execution.v2.Digest",
            "#[derive(Eq, Hash)]",
        )
        .compile(
            &[
                "proto/build/bazel/remote/execution/v2/remote_execution.proto",
                "proto/google/bytestream/bytestream.proto",
            ],
            &["proto"],
        )?;
    Ok(())
}
