mod event;
mod graph;

use crate::event::Event;
use crate::graph::{Graph, NodeId};

use async_trait::async_trait;
use digest::Digest;
use serde::Serialize;
use std::fmt::Debug;
use std::io::Write;
use std::marker::{Send, Sync};
use std::ops::Range;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::OnceLock;
use tokio::runtime::Runtime;
use tokio::sync::mpsc;

#[derive(Debug)]
pub enum ActionError<E> {
    DependencyFailure,
    MissingOutput,
    Internal,
    Execution(E),
}

pub trait Action: Sync {
    fn func_hash(&self) -> Option<&str>;
}

#[async_trait]
pub trait ActionExecutor<A, V, R, E>: Sync {
    /// Invoked for each required action without waiting for inputs.
    /// May return a cached result based on the graph hash.
    async fn pre(
        &self,
        node: &ActionNode<A, R, E>,
        outputs: &[ActionOutput<V>],
    ) -> Option<Result<R, E>>;

    /// Invoked for each action when all inputs are available,
    /// if `pre` returned `None`.
    async fn execute(
        &self,
        node: &ActionNode<A, R, E>,
        inputs: &[ActionInput<V>],
        outputs: &[ActionOutput<V>],
    ) -> Result<R, E>;

    /// Invoked for each action after providing outputs to dependent actions
    /// (or failing). May be used to upload outputs to a remote cache without
    /// blocking dependent actions.
    async fn post(
        &self,
        node: &ActionNode<A, R, E>,
        outputs: &[ActionOutput<V>],
        result: &Result<R, ActionError<E>>,
    );
}

pub struct ActionOutput<V> {
    cell: OnceLock<V>,
}

impl<V> Default for ActionOutput<V> {
    fn default() -> Self {
        ActionOutput {
            cell: OnceLock::new(),
        }
    }
}

#[derive(Debug)]
pub struct ActionInput<V> {
    dependency_id: NodeId,
    output_id: usize,
    cell: OnceLock<V>,
}

impl<V> Default for ActionInput<V> {
    fn default() -> Self {
        ActionInput {
            dependency_id: NodeId::default(),
            output_id: 0,
            cell: OnceLock::new(),
        }
    }
}

impl<V> ActionOutput<V> {
    pub fn get(&self) -> Option<&V> {
        self.cell.get()
    }

    pub fn set(&self, value: V) -> Result<(), V> {
        self.cell.set(value)
    }

    fn is_set(&self) -> bool {
        self.cell.get().is_some()
    }
}

impl<V: Debug> ActionInput<V> {
    pub fn get(&self) -> &V {
        self.cell.get().expect("Action input without value")
    }

    fn set(&self, value: V) {
        self.cell
            .set(value)
            .expect("Action input should only be set once")
    }
}

pub struct ActionNode<A, R, E> {
    graph_hash: Option<String>,

    action: A,

    cost: f32,
    // Maximum distance to target node, includes cost of this node (critical path).
    // None means there is no path to the target node (i.e., not required for the build)
    path_cost: Option<f32>,

    inputs: Range<usize>,
    input_capacity: usize,

    outputs: Range<usize>,

    result: OnceLock<Result<R, ActionError<E>>>,
    ready: Event,

    spawned: AtomicBool,
}

impl<A, R, E> ActionNode<A, R, E> {
    fn new(action: A, inputs: Range<usize>, input_capacity: usize, outputs: Range<usize>) -> Self {
        Self {
            action,
            cost: 1.0,
            path_cost: None,
            graph_hash: None,
            inputs,
            input_capacity,
            outputs,
            result: OnceLock::new(),
            ready: Event::default(),
            spawned: AtomicBool::new(false),
        }
    }

    pub fn action(&self) -> &A {
        &self.action
    }

    pub fn graph_hash(&self) -> Option<&str> {
        self.graph_hash.as_deref()
    }

    pub fn path_cost(&self) -> Option<f32> {
        self.path_cost
    }

    fn is_ok(&self) -> bool {
        match self.result.get() {
            Some(result) => result.is_ok(),
            None => false,
        }
    }
}

pub struct ActionGraph<A, V, R, E> {
    graph: Graph<ActionNode<A, R, E>>,
    outputs: Vec<ActionOutput<V>>,
    inputs: Vec<ActionInput<V>>,
}

impl<A: Action, V: Sync + Send + Clone + Debug, R: Sync + Send + Debug, E: Sync + Send + Debug>
    Default for ActionGraph<A, V, R, E>
{
    fn default() -> Self {
        Self::new()
    }
}

impl<A: Action, V: Sync + Send + Clone + Debug, R: Sync + Send + Debug, E: Sync + Send + Debug>
    ActionGraph<A, V, R, E>
{
    pub fn new() -> Self {
        Self {
            graph: Graph::new(),
            outputs: vec![],
            inputs: vec![],
        }
    }

    pub fn add_action(&mut self, action: A, n_inputs: usize, n_outputs: usize) -> NodeId {
        let inputs_offset = self.inputs.len();
        self.inputs
            .resize_with(inputs_offset + n_inputs, Default::default);
        let outputs_offset = self.outputs.len();
        self.outputs
            .resize_with(outputs_offset + n_outputs, Default::default);
        self.graph.add_node(
            ActionNode::new(
                action,
                inputs_offset..inputs_offset,
                n_inputs,
                outputs_offset..outputs_offset + n_outputs,
            ),
            n_inputs,
        )
    }

    pub fn add_dependency(
        &mut self,
        dependent_id: NodeId,
        dependency_id: NodeId,
        output_id: usize,
    ) -> usize {
        self.graph.add_edge(dependency_id, dependent_id);
        let dependent = &mut self.graph[dependent_id];
        let input_id = dependent.inputs.len();
        assert!(input_id < dependent.input_capacity);
        dependent.inputs.end += 1;
        let input = &mut self.inputs[dependent.inputs.start + input_id];
        input.dependency_id = dependency_id;
        input.output_id = output_id;
        input_id
    }

    fn calculate_costs(&mut self, topsort: &[NodeId], toplevel_node_id: NodeId) {
        let toplevel_node = &mut self.graph[toplevel_node_id];
        toplevel_node.path_cost = Some(toplevel_node.cost);

        for &node_id in topsort.iter().rev() {
            if let Some(node_path_cost) = self.graph[node_id].path_cost {
                self.graph.for_each_mut_in_neighbor(node_id, |dep| {
                    let path_cost = Some(node_path_cost + dep.cost);
                    if path_cost > dep.path_cost {
                        dep.path_cost = path_cost;
                    }
                });
            }
        }
    }

    fn calculate_graph_hashes(
        &mut self,
        topsort: &[NodeId],
        global_hash: Option<&str>,
    ) -> Result<(), bincode::Error> {
        let mut buffer = Vec::<u8>::new();

        for &node_id in topsort {
            let node = &self.graph[node_id];

            assert!(node.graph_hash.is_none());
            assert!(buffer.is_empty());

            let inputs = &self.inputs[node.inputs.clone()];

            if let Some(func_hash) = node.action.func_hash() {
                if inputs
                    .iter()
                    .any(|input| self.graph[input.dependency_id].graph_hash.is_none())
                {
                    // Action is not cacheable
                    continue;
                }

                let mut serializer = bincode::Serializer::new(buffer.by_ref(), bincode::options());

                if let Some(hash) = global_hash {
                    "global".serialize(&mut serializer)?;
                    hash.serialize(&mut serializer)?;
                }

                "func".serialize(&mut serializer)?;
                func_hash.serialize(&mut serializer)?;

                "dependencies".serialize(&mut serializer)?;
                self.graph.in_degree(node_id).serialize(&mut serializer)?;
                for input in inputs {
                    let dep_hash = self.graph[input.dependency_id]
                        .graph_hash
                        .as_ref()
                        .expect("Dependency should have graph hash");
                    dep_hash.serialize(&mut serializer)?;
                    input.output_id.serialize(&mut serializer)?;
                }

                let mut hasher = sha2::Sha256::new();
                let data: &[u8] = buffer.as_ref();
                hasher.update(data);
                buffer.clear();

                let node = &mut self.graph[node_id];
                node.graph_hash = Some(hex::encode(hasher.finalize()))
            }
        }

        Ok(())
    }

    fn get_output(&self, node_id: NodeId, output_id: usize) -> &V {
        let node = &self.graph[node_id];
        assert!(output_id < node.outputs.len());
        self.outputs[node.outputs.start + output_id]
            .get()
            .expect("All outputs should be set")
    }

    async fn run_node(
        &self,
        spawn_tx: mpsc::Sender<NodeId>,
        executor: &dyn ActionExecutor<A, V, R, E>,
        node_id: NodeId,
    ) {
        let node = &self.graph[node_id];

        // Panic guard to ensure dependents are not waiting indefinitely
        // for failed node.
        struct Guard<'a, A, R: Debug, E: Debug> {
            node: &'a ActionNode<A, R, E>,
        }

        impl<A, R: Debug, E: Debug> Drop for Guard<'_, A, R, E> {
            fn drop(&mut self) {
                if !self.node.ready.get() {
                    if self.node.result.get().is_none() {
                        self.node
                            .result
                            .set(Err(ActionError::Internal))
                            .expect("ActionNode result should only be set once");
                    }
                    self.node.ready.set();
                }
            }
        }
        let _guard = Guard { node };

        let inputs = &self.inputs[node.inputs.clone()];
        let outputs = &self.outputs[node.outputs.clone()];

        let mut exec_result = executor.pre(node, outputs).await;

        if exec_result.is_none() {
            // Not cached, we need the inputs for execution

            for dep_id in self.graph.iter_in_neighbor_ids(node_id) {
                if !self.graph[dep_id].spawned.swap(true, Ordering::Relaxed) {
                    spawn_tx
                        .send(dep_id)
                        .await
                        .expect("Spawn receiver should still be alive");
                }
            }

            for input in inputs {
                let dep = &self.graph[input.dependency_id];
                dep.ready.wait().await;

                if dep.is_ok() {
                    input.set(
                        self.get_output(input.dependency_id, input.output_id)
                            .clone(),
                    );
                } else {
                    node.result
                        .set(Err(ActionError::DependencyFailure))
                        .expect("ActionNode result should only be set once");
                    break;
                }
            }

            if node.result.get().is_none() {
                exec_result = Some(executor.execute(node, inputs, outputs).await);
            }
        }

        if node.result.get().is_none() {
            node.result
                .set(match exec_result.expect("Result should be set") {
                    Ok(v) => {
                        if outputs.iter().any(|output| !output.is_set()) {
                            Err(ActionError::MissingOutput)
                        } else {
                            Ok(v)
                        }
                    }
                    Err(e) => Err(ActionError::Execution(e)),
                })
                .expect("ActionNode result should only be set once");
        }

        node.ready.set();

        executor
            .post(
                node,
                outputs,
                node.result.get().expect("ActionNode result should be set"),
            )
            .await;
    }

    pub fn run(
        mut self,
        runtime: &Runtime,
        target_node: NodeId,
        executor: &dyn ActionExecutor<A, V, R, E>,
        global_hash: Option<&str>,
    ) {
        let topsort = self.graph.topsort();
        self.calculate_costs(&topsort, target_node);
        self.calculate_graph_hashes(&topsort, global_hash)
            .expect("Unexpected failure in graph hash calculation");

        let (spawn_tx, mut spawn_rx) = mpsc::channel::<NodeId>(64);

        let _guard = runtime.enter();

        async_scoped::TokioScope::scope_and_block(|s| {
            // We need to keep a Sender around to spawn additional `run_node` tasks,
            // however, we don't want that Sender to keep the mpsc channel alive,
            // otherwise it's an endless loop.
            let weak_spawn_tx = spawn_tx.downgrade();

            s.spawn(self.run_node(spawn_tx, executor, target_node));

            while let Some(dep_id) = spawn_rx.blocking_recv() {
                s.spawn(
                    self.run_node(
                        weak_spawn_tx
                            .upgrade()
                            .expect("Sender instance of dependent action should still be alive"),
                        executor,
                        dep_id,
                    ),
                );
            }
        });
    }
}
