use std::collections::VecDeque;
use std::ops::{Index, IndexMut, Range};

#[derive(Clone, Copy, Debug, Default, Eq, Hash, PartialEq)]
pub struct NodeId(pub usize);

struct Node<N> {
    in_edges: Range<usize>,
    in_capacity: usize,
    out_degree: usize,
    data: N,
}

/// Directed acyclic graph
pub struct Graph<N> {
    nodes: Vec<Node<N>>,
    edges: Vec<NodeId>,
}

impl<N> Graph<N> {
    pub const fn new() -> Self {
        Graph {
            nodes: vec![],
            edges: vec![],
        }
    }

    pub fn add_node(&mut self, data: N, in_capacity: usize) -> NodeId {
        let in_edges = self.edges.len()..self.edges.len();
        self.edges
            .resize_with(in_edges.start + in_capacity, Default::default);
        self.nodes.push(Node {
            out_degree: 0,
            in_edges,
            in_capacity,
            data,
        });
        NodeId(self.nodes.len() - 1)
    }

    /// Add edge from specified origin to specified destination
    pub fn add_edge(&mut self, origin_id: NodeId, destination_id: NodeId) {
        if self
            .iter_in_neighbor_ids(destination_id)
            .any(|neighbor| neighbor == origin_id)
        {
            // Edge already exists
            return;
        }

        let destination = &mut self.nodes[destination_id.0];
        assert!(destination.in_edges.len() < destination.in_capacity);

        let edge_id = destination.in_edges.end;
        self.edges[edge_id] = origin_id;
        destination.in_edges.end += 1;

        let origin = &mut self.nodes[origin_id.0];
        origin.out_degree += 1;
    }

    pub fn iter_leaf_node_ids(&self) -> impl Iterator<Item = NodeId> + '_ {
        self.nodes.iter().enumerate().filter_map(|(idx, node)| {
            if node.out_degree == 0 {
                Some(NodeId(idx))
            } else {
                None
            }
        })
    }

    pub fn iter_in_neighbor_ids(
        &self,
        destination_id: NodeId,
    ) -> impl Iterator<Item = NodeId> + '_ {
        let node = &self.nodes[destination_id.0];
        self.edges[node.in_edges.clone()].iter().copied()
    }

    pub fn for_each_mut_in_neighbor<F>(&mut self, destination_id: NodeId, mut f: F)
    where
        F: FnMut(&mut N),
    {
        let node = &self.nodes[destination_id.0];
        for &neighbor_id in self.edges[node.in_edges.clone()].iter() {
            f(&mut self.nodes[neighbor_id.0].data);
        }
    }

    pub fn node_count(&self) -> usize {
        self.nodes.len()
    }

    pub fn in_degree(&self, node_id: NodeId) -> usize {
        self.nodes[node_id.0].in_edges.len()
    }

    pub fn out_degree(&self, node_id: NodeId) -> usize {
        self.nodes[node_id.0].out_degree
    }

    pub fn topsort(&self) -> Vec<NodeId> {
        let mut result = Vec::<NodeId>::with_capacity(self.node_count());

        let mut start_nodes: VecDeque<NodeId> = VecDeque::new();

        start_nodes.extend(self.iter_leaf_node_ids());

        let mut n_processed_outgoing: Vec<usize> = vec![0; self.node_count()];

        while let Some(destination_id) = start_nodes.pop_front() {
            for origin_id in self.iter_in_neighbor_ids(destination_id) {
                let out_degree = self.out_degree(origin_id);
                n_processed_outgoing[origin_id.0] += 1;
                if n_processed_outgoing[origin_id.0] == out_degree {
                    start_nodes.push_back(origin_id);
                } else {
                    assert!(n_processed_outgoing[origin_id.0] < out_degree);
                }
            }

            result.push(destination_id);
        }

        for (node_id, &n_processed) in n_processed_outgoing.iter().enumerate() {
            let out_degree = self.out_degree(NodeId(node_id));
            if n_processed < out_degree {
                panic!("Graph is cyclic");
            }
        }

        result.reverse();

        result
    }
}

impl<N> Index<NodeId> for Graph<N> {
    type Output = N;

    fn index(&self, index: NodeId) -> &N {
        &self.nodes[index.0].data
    }
}

impl<N> IndexMut<NodeId> for Graph<N> {
    fn index_mut(&mut self, index: NodeId) -> &mut N {
        &mut self.nodes[index.0].data
    }
}
